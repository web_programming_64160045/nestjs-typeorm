import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Product {
    @PrimaryGeneratedColumn({name: "product_ids"})
    id: number;

    @Column({name: "product_name"})
    name: string;

    @Column({name: "product_price"})
    price: number;
}
import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productsRepository = AppDataSource.getRepository(Product)

    const products = await productsRepository.find()
    console.log("Loaded products: ", products)

    const updatedproduct = await productsRepository.findOneBy({id:1})
    console.log(updatedproduct)
    updatedproduct.price=80
    await productsRepository.save(updatedproduct)
    
}).catch(error => console.log(error))
